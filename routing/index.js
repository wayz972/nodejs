const express = require('express');
const router = express.Router();
const {homePage,contact,Redirect,services,todo,Addtodo,todoId,Delete} = require('../controllers/app.controllers');


//api
router.get('/api/todo',todo);
router.get('/api/todo/:id',todoId);
router.post('/api/todo',Addtodo);
router.delete('/api/delete/:id',Delete);
//ROUTING
    router.get('/', homePage);
    router.get('/contact',contact);
   
    router.get('/services/:userName',services);
    router.get('*', Redirect);
       
          module.exports = router;