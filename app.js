const express = require('express');
const {resolve}=require("path");
const router=require('./routing');
const app = express();
//CONFIG
app.set('view engine','pug')
app.use(express.static(resolve('static')));
app.use(router);

module.exports=app;